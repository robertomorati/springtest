package com.morati.springtest.controller;

import com.morati.springtest.mongo.User;
import com.morati.springtest.mongo.UserService;
import org.apache.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class RestUserController {

    private static Logger log = Logger.getLogger(com.morati.springtest.controller.UserController.class);

    @Resource(name = "userService")
    private UserService userService;

    @GetMapping(value="/users", produces = "application/json")
    private ResponseEntity<List<User>> all(){
        return ResponseEntity.ok(userService.getAllUsers());
    }

    @PutMapping(value="/users/create/")
    public ResponseEntity createUser(@RequestBody User newUser, @PathVariable UUID id) {
        log.debug("Creating new user: " + newUser);
        userService.save(newUser);
        return ResponseEntity.ok().build();
    }

    @PutMapping(value="/users/update/{id}")
    public ResponseEntity updateUser(@RequestBody User newUser, @PathVariable UUID id) {
        log.debug("Updating user: " + newUser);
        userService.save(newUser);
        return ResponseEntity.ok().build();
    }

    //Example request curl -X DELETE http://localhost:8080/api/v1/users/c1baa7bd-9028-4c1f-8e03-d28855f86696
    @DeleteMapping(value="/users/{id}")
    public ResponseEntity delete(@PathVariable String id){

        if(!userService.checkUserExists(id)){
            log.debug("Id " + id + " is not existed");
            return ResponseEntity.badRequest().build();
        }
        userService.deleteUser(id);

        return ResponseEntity.ok().build();
    }
}
