package com.morati.springtest.controller;


import java.util.List;
import java.util.UUID;

import org.apache.log4j.Logger;
import javax.annotation.Resource;

import com.morati.springtest.mongo.User;
import com.morati.springtest.mongo.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


@Controller
@RequestMapping("/user")
public class UserController {

    private static Logger log = Logger.getLogger(UserController.class);

    // https://stackoverflow.com/questions/50419330/meaning-of-resource-annotation
    @Resource(name = "userService")
    private UserService userService;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String getPersons(Model model) {
        log.debug("Request to fetch all users from the mongo database");

        List user_list = userService.getAllUsers();
        model.addAttribute("users", user_list);
        return "/welcome";
    }

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String add(Model model) {
        log.debug("Request to open the new user form page");
        model.addAttribute("userAttr", new User());
        return "/form";
    }

    @RequestMapping(value = "/update", method = RequestMethod.GET)
    public String edit(@RequestParam(value = "id", required = true) String id, Model model) {
        log.debug("Request to open the edit user form page" );
        model.addAttribute("userAttr", userService.getUserById(id));
        return "/form";
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String delete(@RequestParam(value = "id", required = true) String id, Model model) {
        userService.deleteUser(id);
        return "redirect:list";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String save(@ModelAttribute("userAttr") User user) {
        userService.save(user);
        return "redirect:list";
    }
}