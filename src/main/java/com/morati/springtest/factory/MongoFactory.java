package com.morati.springtest.factory;

import org.apache.log4j.Logger;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.Mongo;
import com.mongodb.MongoException;

public class MongoFactory{

    private static Logger log = Logger.getLogger(MongoFactory.class);

    private static Mongo mongoInstance;

    private MongoFactory() {}


    public static Mongo getMongoInstance(){
        int port_number = 27017;
        String hostname = "localhost";

        if(mongoInstance == null){
            try{
                mongoInstance = new Mongo(hostname, port_number);
            }catch( MongoException ex){
                log.error(ex);
            }
        }
        return mongoInstance;
    }

    public static DB getMongoDatabase(String db_name){
        return getMongoInstance().getDB(db_name);
    }

    public static DBCollection getCollection(String db_name, String db_collection){
        return getMongoDatabase(db_name).getCollection(db_collection);
    }

}



