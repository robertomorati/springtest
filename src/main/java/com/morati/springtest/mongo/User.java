package com.morati.springtest.mongo;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.annotation.Generated;
import java.io.Serializable;
import java.util.UUID;

@Data
@Document(collection = "users")
public class User implements Serializable{

    @Id
    private String id = UUID.randomUUID().toString();
    private String name;
    private String username;

    public User(){
        super();
    }

    public User(String name, String username){
        super();
        this.name = name;
        this.username = username;
    }
}