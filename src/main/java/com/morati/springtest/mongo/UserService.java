package com.morati.springtest.mongo;

import com.morati.springtest.repository.UserRepository;
import org.apache.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;


@Service("userService")
@Transactional
public class UserService{

    // static String db_name = "mydb", db_collection = "users";
    private static Logger log = Logger.getLogger(UserService.class);

    @Autowired
    private UserRepository userRepository;

    public void save(User user) {
        log.debug("Creating or Updating new user." + user.getId());
        userRepository.save(user);
    }

    public List<User> getAllUsers() {
        log.debug("Retrieving all users.");
        List<User>  user_list = userRepository.findAll();
        return user_list;
    }

    public User getUserById(String id){
        log.debug("Retrieving the user with Id:" + id);
        return userRepository.findById(id).get();
    }

    public boolean checkUserExists(String id){
        return userRepository.findById(id) != null;
    }

    public void deleteUser(String id) {
        log.debug("Deleting the user with  Id:" + id);
        userRepository.deleteById(id);
    }

    /**
    public List getAll(){

        log.debug("Fetching all users from mongodb.");

        List user_list = new ArrayList();
        DBCollection collection = MongoFactory.getCollection(db_name, db_collection);

        DBCursor cursor = collection.find();
        while(cursor.hasNext()){
            DBObject dbObject = cursor.next();
            user_list.add(new User(dbObject.get("name").toString()));
        }
        return user_list;
    }


    public Boolean add(User user){
        boolean output = false;
        Random ran = new Random();
        log.debug("Adding a new user to mongodb. The user name is= " + user.getName());

        try{
            DBCollection collection = MongoFactory.getCollection(db_name, db_collection);

            BasicDBObject document = new BasicDBObject();
            // document.put("id", user.getId());
            document.put("name", user.getName());
            collection.insert(document);
            output = true;
        }catch (Exception e){
            output = false;
            log.error("An error occurred while saving the new user  into mongodb", e);
        }
        return output;
    }

    public Boolean edit(User user) {
        boolean output = false;
        log.debug("Updating user in mongodb. The user id is= " + user.getId());
        try {
            BasicDBObject existingUser= (BasicDBObject) getDBObject(user.getId());
            DBCollection collection = MongoFactory.getCollection(db_name, db_collection);

            BasicDBObject edited = new BasicDBObject();
            // edited.put("id", user.getId().toString());
            edited.put("name", user.getName());
            collection.update(existingUser, edited);
            output = true;
        } catch (Exception e) {
            output = false;
            log.error("An error has occurred while updating an existing user to the mongodb", e);
        }
        return output;
    }

    public Boolean delete(String id) {
        boolean output = false;
        log.debug("Deleting an existing user from the mongo database; Entered user_id is= " + id);
        try {

            BasicDBObject item = (BasicDBObject) getDBObject(id);

            DBCollection collection = MongoFactory.getCollection(db_name, db_collection);

            collection.remove(item);
            output = true;
        } catch (Exception e) {
            output = false;
            log.error("An error occurred while deleting an existing user from the mongo database", e);
        }
        return output;
    }

    private DBObject getDBObject(String id) {
        DBCollection collection = MongoFactory.getCollection(db_name, db_collection);

        DBObject dbObject = new BasicDBObject();

        dbObject.put("id", id.toString());

        return  collection.findOne(dbObject);
    }

    public boolean checkUserExists(String id){
        DBCollection collection = MongoFactory.getCollection(db_name, db_collection);

        DBObject dbObject = new BasicDBObject();

        dbObject.put("id", id.toString());

        return  collection.findOne(dbObject) != null;
    }

    public User findUserById(String id) {

        DBCollection collection = MongoFactory.getCollection(db_name, db_collection);

        DBObject dbObject = new BasicDBObject();
        dbObject.put("id", id.toString());

        DBObject document = collection.findOne(dbObject);

        User user = new User(document.get("name").toString());

        return user;
    }**/
}