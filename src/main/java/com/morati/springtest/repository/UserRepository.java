package com.morati.springtest.repository;

import com.morati.springtest.mongo.User;
import org.springframework.data.mongodb.repository.MongoRepository;


public interface UserRepository extends MongoRepository <User, String>{
}
