<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>User form</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">


    </head>
    <body>
        <div class="container bg-dark">
            <h3 id="form_header"  align="center" class="text-light bg-dark">Create or Update User</h3>
            <div> </div>

            <!-- User input form to add a new user or update the existing user-->
            <c:url var="saveUrl" value="/user/save" />
            <form:form id="user_form" modelAttribute="userAttr" method="POST" action="${saveUrl}">
                <form:hidden path="id" />
                    <label class="font-weight-bold text-light bg-dark" for="user_name">Name: </label>
                    <form:input id="user_name" cssClass="form-control text-light bg-dark" path="name" />
                     <label class="font-weight-bold text-light bg-dark" for="user_name">Username: </label>
                     <form:input id="user_username" cssClass="form-control text-light bg-dark" path="username" />
                <div> </div>
                <button id="saveBtn" type="submit" class="btn btn-primary">Save</button>
            </form:form>
        </div>
    </body>
</html>